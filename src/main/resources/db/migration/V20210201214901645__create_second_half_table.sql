create table second_word
(
    second_id      int8,
    common_part_id int8
);

insert into second_word (second_id, common_part_id)
SELECT second.id, cp.id as common_part_id
from common_parts cp
         inner join words second on position(cp.common_part in second.word) = 1;