INSERT INTO common_parts (id, common_part)
SELECT nextval('common_parts_seq'), suffix
from (SELECT DISTINCT substring(word from gs.i for length(word)) as suffix
      from words,
           generate_series(2, length(word) - 1) gs(i)) as wgs;