create table jabberwocky_rates
(
    jabberwocky_id int8         not null,
    user_id        varchar(255) not null,
    is_positive    boolean      not null,
    rate_date_time timestamp    not null,
    primary key (jabberwocky_id, user_id)
);

alter table if exists jabberwocky_words
    add column negative_rates int8,
    add column positive_rates int8;

update jabberwocky_words
set negative_rates = 0,
    positive_rates = 0;

alter table if exists jabberwocky_words
    alter column negative_rates set not null,
    alter column positive_rates set not null;
