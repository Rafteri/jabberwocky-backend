INSERT INTO jabberwocky_words (id, common_part_id, first_id, second_id)
SELECT nextval('jabberwocky_words_seq'), jw.cp_id, jw.f_id, jw.s_id
from (select fw.common_part_id as cp_id, fw.first_id as f_id, sw.second_id as s_id
      from first_word fw
               inner join second_word sw on fw.common_part_id = sw.common_part_id) as jw;