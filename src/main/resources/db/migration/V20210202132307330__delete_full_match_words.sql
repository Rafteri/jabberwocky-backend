delete
from jabberwocky_words
where id in (select jw.id
             from jabberwocky_words jw
                      left join common_parts cp on cp.id = jw.common_part_id
                      left join words fw on fw.id = jw.first_id
                      left join words sw on sw.id = jw.second_id
             where fw.word = cp.common_part
                or sw.word = cp.common_part
);