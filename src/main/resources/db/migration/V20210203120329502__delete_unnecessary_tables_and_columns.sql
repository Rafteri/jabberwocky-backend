drop table first_word;
drop table second_word;
alter table words
    drop column reverse_word;
alter table common_parts
    drop column reverse_common_part;