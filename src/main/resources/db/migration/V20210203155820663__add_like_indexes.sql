CREATE EXTENSION pg_trgm;

CREATE INDEX trgm_idx_jabberwocky_word_combos ON jabberwocky_words USING gin (combo gin_trgm_ops);