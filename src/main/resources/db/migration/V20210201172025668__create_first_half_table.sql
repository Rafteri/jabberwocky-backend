create table first_word
(
    first_id       int8,
    common_part_id int8
);

alter table words
    add column reverse_word varchar(256);
update words
set reverse_word = reverse(word);

alter table common_parts
    add column reverse_common_part varchar(256);
update common_parts
set reverse_common_part = reverse(common_part);

insert into first_word (first_id, common_part_id)
SELECT first.id, cp.id as common_part_id
from common_parts cp
         inner join words first on position(cp.reverse_common_part in first.reverse_word) = 1;