UPDATE jabberwocky_words
SET combo = subquery.combo
FROM (SELECT jw.id, substring(fw.word from 1 for (length(fw.word) - length(cp.common_part))) || sw.word as combo
      FROM jabberwocky_words jw
               left join common_parts cp on cp.id = jw.common_part_id
               left join words fw on fw.id = jw.first_id
               left join words sw on sw.id = jw.second_id
     ) AS subquery
WHERE jabberwocky_words.id = subquery.id;