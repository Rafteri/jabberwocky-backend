
    create table common_parts (
       id int8 not null,
        common_part varchar(255),
        primary key (id)
    );

    create table jabberwocky_words (
       id int8 not null,
        common_part_id int8,
        first_id int8,
        second_id int8,
        primary key (id)
    );

    create table words (
       id int8 not null,
        word varchar(255),
        primary key (id)
    );
create sequence common_parts_seq start 1 increment 1;
create sequence jabberwocky_words_seq start 1 increment 1;
create sequence words_seq start 1 increment 1;

    alter table if exists jabberwocky_words 
       add constraint FKbnrnbtooq00j7pmvf7132as03 
       foreign key (common_part_id) 
       references common_parts;

    alter table if exists jabberwocky_words 
       add constraint FKgrccj0kdrbpaok4xp9afwsoav 
       foreign key (first_id) 
       references words;

    alter table if exists jabberwocky_words 
       add constraint FKnrddbope4o4nm8thhifyodice 
       foreign key (second_id) 
       references words;
