package com.rafteri.jabberwocky.mappers;

import com.rafteri.jabberwocky.dto.WordDto;
import com.rafteri.jabberwocky.models.Word;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

/**
 * 03.02.2021
 *
 * @author Dinar Rafikov
 */
@Component
public class WordMapper {

    @Nullable
    public WordDto apply(@Nullable final Word word) {
        if (word == null) {
            return null;
        }
        return WordDto.builder()
                .id(word.getId())
                .word(word.getWord())
                .definition(word.getDefinition())
                .build();
    }
}
