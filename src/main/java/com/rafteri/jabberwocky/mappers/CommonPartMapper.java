package com.rafteri.jabberwocky.mappers;

import com.rafteri.jabberwocky.dto.CommonPartDto;
import com.rafteri.jabberwocky.models.CommonPart;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

/**
 * 03.02.2021
 *
 * @author Dinar Rafikov
 */
@Component
public class CommonPartMapper {

    @Nullable
    public CommonPartDto apply(@Nullable final CommonPart commonPart) {
        if (commonPart == null) {
            return null;
        }
        return CommonPartDto.builder()
                .id(commonPart.getId())
                .part(commonPart.getCommonPart())
                .build();
    }
}
