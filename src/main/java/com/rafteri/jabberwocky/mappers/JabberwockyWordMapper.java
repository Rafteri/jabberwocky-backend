package com.rafteri.jabberwocky.mappers;

import com.rafteri.jabberwocky.dto.JabberwockyDto;
import com.rafteri.jabberwocky.models.JabberwockyWord;
import lombok.RequiredArgsConstructor;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

/**
 * 03.02.2021
 *
 * @author Dinar Rafikov
 */
@Component
@RequiredArgsConstructor
public class JabberwockyWordMapper {

    private final WordMapper wordMapper;
    private final CommonPartMapper commonPartMapper;

    @Nullable
    public JabberwockyDto apply(@Nullable final JabberwockyWord jabberwocky) {
        if (jabberwocky == null) {
            return null;
        }
        return JabberwockyDto.builder()
                .id(jabberwocky.getId())
                .first(wordMapper.apply(jabberwocky.getFirst()))
                .second(wordMapper.apply(jabberwocky.getSecond()))
                .common(commonPartMapper.apply(jabberwocky.getCommonPart()))
                .result(jabberwocky.getCombo())
                .positiveRates(jabberwocky.getPositiveRates())
                .negativeRates(jabberwocky.getNegativeRates())
                .build();
    }
}
