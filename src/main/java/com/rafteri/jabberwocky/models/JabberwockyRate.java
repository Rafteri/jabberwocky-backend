package com.rafteri.jabberwocky.models;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 18.02.2021
 *
 * @author Dinar Rafikov
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@IdClass(JabberwockyRate.Ids.class)
@Table(name = "jabberwocky_rates")
public class JabberwockyRate {

    @Id
    @Column(nullable = false)
    private Long jabberwockyId;

    @Id
    @Column(nullable = false)
    private String userId;

    @Column(nullable = false)
    private boolean isPositive;

    @Column(nullable = false)
    private LocalDateTime rateDateTime;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Ids implements Serializable {
        private Long jabberwockyId;
        private String userId;
    }
}
