package com.rafteri.jabberwocky.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * 30.01.2021
 *
 * @author Dinar Rafikov
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "words")
@SequenceGenerator(name = AbstractEntity.GENERATOR, sequenceName = "words_seq", allocationSize = 1)
public class Word extends AbstractEntity {
    private String word;

    @Column(columnDefinition = "VARCHAR")
    private String definition;
}
