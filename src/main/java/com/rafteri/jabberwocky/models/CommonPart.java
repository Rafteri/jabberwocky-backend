package com.rafteri.jabberwocky.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * 30.01.2021
 *
 * @author Dinar Rafikov
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "common_parts")
@SequenceGenerator(name = AbstractEntity.GENERATOR, sequenceName = "common_parts_seq", allocationSize = 1)
public class CommonPart extends AbstractEntity {
    private String commonPart;
}
