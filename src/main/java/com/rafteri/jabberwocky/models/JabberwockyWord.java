package com.rafteri.jabberwocky.models;

import lombok.*;

import javax.persistence.*;

/**
 * 30.01.2021
 *
 * @author Dinar Rafikov
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "jabberwocky_words")
@SequenceGenerator(name = AbstractEntity.GENERATOR, sequenceName = "jabberwocky_words_seq", allocationSize = 1)
public class JabberwockyWord extends AbstractEntity {

    @ManyToOne
    private Word first;

    @ManyToOne
    private Word second;

    @ManyToOne
    private CommonPart commonPart;

    private String combo;
    private long positiveRates = 0L;
    private long negativeRates = 0L;

    public double getCalcRate() {
        return (positiveRates * 2. + negativeRates) / (positiveRates + negativeRates);
    }

    public int getCommonPartLength() {
        return commonPart.getCommonPart().length();
    }
}
