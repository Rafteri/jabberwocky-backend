package com.rafteri.jabberwocky.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 03.02.2021
 *
 * @author Dinar Rafikov
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WordDto {
    private Long id;
    private String word;
    private String definition;
}
