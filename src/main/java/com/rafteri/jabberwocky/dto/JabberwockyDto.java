package com.rafteri.jabberwocky.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 03.02.2021
 *
 * @author Dinar Rafikov
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JabberwockyDto {
    private Long id;
    private WordDto first;
    private WordDto second;
    private CommonPartDto common;
    private String result;
    private Long positiveRates;
    private Long negativeRates;
}
