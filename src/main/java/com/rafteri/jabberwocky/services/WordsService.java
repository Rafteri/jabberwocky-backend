package com.rafteri.jabberwocky.services;

import com.rafteri.jabberwocky.exceptions.NotFoundException;
import com.rafteri.jabberwocky.repositories.WordsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * 03.02.2021
 *
 * @author Dinar Rafikov
 */
@Service
@RequiredArgsConstructor
public class WordsService {

    private final WordsRepository wordsRepository;

    public String getDefinition(final String word) {
        return wordsRepository.findDefinition(word)
                .orElseThrow(NotFoundException::new);
    }

    public String getDefinition(final Long id) {
        return wordsRepository.findDefinition(id)
                .orElseThrow(NotFoundException::new);
    }
}
