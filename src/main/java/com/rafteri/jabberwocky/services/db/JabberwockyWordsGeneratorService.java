package com.rafteri.jabberwocky.services.db;

import com.rafteri.jabberwocky.models.CommonPart;
import com.rafteri.jabberwocky.models.JabberwockyWord;
import com.rafteri.jabberwocky.models.Word;
import com.rafteri.jabberwocky.repositories.CommonPartsRepository;
import com.rafteri.jabberwocky.repositories.JabberwockyWordsRepository;
import com.rafteri.jabberwocky.repositories.WordsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * 01.02.2021
 *
 * @author Dinar Rafikov
 */
@Service
@RequiredArgsConstructor
public class JabberwockyWordsGeneratorService {
    private final WordsRepository wordsRepository;
    private final CommonPartsRepository commonPartsRepository;
    private final JabberwockyWordsRepository jabberwockyWordsRepository;

    @PostConstruct
    public void generate() {
//        final List<JabberwockyWord> jabberwockyWords = new ArrayList<>();
//        final List<Word> words = wordsRepository.findAll();
//        final List<CommonPart> parts = commonPartsRepository.findAll();
//        int i = 0;
//        for (final CommonPart part : parts) {
//            i++;
//            for (final Word first : words) {
//                if (first.getWord().endsWith(part.getCommonPart())) {
////                    for (final Word second : words) {
////                        if (first.getWord().startsWith(part.getCommonPart())) {
//                            jabberwockyWords.add(
//                                    JabberwockyWord.builder()
//                                            .first(first)
////                                            .second(second)
//                                            .commonPart(part)
//                                            .build()
//                            );
////                        }
////                    }
//                }
//            }
//            System.out.printf("Completed %f.2\n", (float)i / parts.size());
//        }
//        jabberwockyWordsRepository.saveAll(jabberwockyWords);
    }
}
