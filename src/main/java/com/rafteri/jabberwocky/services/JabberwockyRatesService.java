package com.rafteri.jabberwocky.services;

import com.rafteri.jabberwocky.exceptions.BadRequestException;
import com.rafteri.jabberwocky.models.JabberwockyRate;
import com.rafteri.jabberwocky.repositories.JabberwockyRatesRepository;
import com.rafteri.jabberwocky.repositories.JabberwockyWordsRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Optional;

/**
 * 18.02.2021
 *
 * @author Dinar Rafikov
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class JabberwockyRatesService {

    private final JabberwockyRatesRepository ratesRepository;
    private final JabberwockyWordsRepository wordsRepository;

    @Transactional
    public void rate(final Long id, final String userId, final boolean isPositive) {
        // TODO: 18.02.2021 refactor
        final Optional<JabberwockyRate> rateOptional = ratesRepository.findById(new JabberwockyRate.Ids(id, userId));
        long posInterest = 0;
        long negInterest = 0;
        if (rateOptional.isPresent()) {
            if (rateOptional.get().isPositive() == isPositive) {
                throw new BadRequestException("duplicate");
            } else {
                if (isPositive) {
                    negInterest--;
                    posInterest++;
                } else {
                    posInterest--;
                    negInterest++;
                }
            }
        } else {
            if (isPositive) {
                posInterest++;
            } else {
                negInterest++;
            }
        }
        ratesRepository.save(new JabberwockyRate(id, userId, isPositive, LocalDateTime.now()));
        wordsRepository.addRates(id, posInterest, negInterest);
    }
}
