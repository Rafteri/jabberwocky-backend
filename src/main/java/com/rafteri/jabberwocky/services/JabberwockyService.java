package com.rafteri.jabberwocky.services;

import com.rafteri.jabberwocky.exceptions.NotFoundException;
import com.rafteri.jabberwocky.models.JabberwockyWord;
import com.rafteri.jabberwocky.repositories.JabberwockyWordsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 03.02.2021
 *
 * @author Dinar Rafikov
 */
@Service
@RequiredArgsConstructor
public class JabberwockyService {
    private static final int RANDOM_SELECT_COUNT = 20;

    private final JabberwockyWordsRepository jabberwockyRepository;

    private Long minId;
    private Long maxId;
    private Random random;

    @PostConstruct
    public void init() {
        minId = jabberwockyRepository.lowestId();
        maxId = jabberwockyRepository.greatestId();
        random = new Random();
    }

    public JabberwockyWord getRandom() {
        final Queue<JabberwockyWord> words = new PriorityQueue<>(
                RANDOM_SELECT_COUNT,
                Comparator.comparingDouble(JabberwockyWord::getCalcRate).reversed()
                        .thenComparingInt(JabberwockyWord::getCommonPartLength).reversed()
        );
        words.addAll(
                jabberwockyRepository.findAllById(
                        random.longs(RANDOM_SELECT_COUNT, minId, maxId + 1)
                                .boxed()
                                .collect(Collectors.toList())
                )
        );
        return words.peek();
    }

    public Page<JabberwockyWord> search(final String word, final @Min(value = 1) @Max(value = 100) Integer count, final @Min(value = 0) Integer page) {
        return jabberwockyRepository.findByComboLike(
                "%" + word + "%",
                PageRequest.of(page, count)
        );
    }

    public JabberwockyWord get(final Long id) {
        return jabberwockyRepository.findById(id)
                .orElseThrow(NotFoundException::new);
    }
}
