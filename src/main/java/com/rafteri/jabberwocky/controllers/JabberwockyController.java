package com.rafteri.jabberwocky.controllers;

import com.rafteri.jabberwocky.dto.JabberwockyDto;
import com.rafteri.jabberwocky.mappers.JabberwockyWordMapper;
import com.rafteri.jabberwocky.models.JabberwockyWord;
import com.rafteri.jabberwocky.services.JabberwockyRatesService;
import com.rafteri.jabberwocky.services.JabberwockyService;
import com.rafteri.jabberwocky.validation.JabberwockyWordExists;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.stream.Collectors;

/**
 * 03.02.2021
 *
 * @author Dinar Rafikov
 */
@RestController
@RequestMapping("/jabberwocky")
@RequiredArgsConstructor
public class JabberwockyController {

    private final JabberwockyService jabberwockyService;
    private final JabberwockyRatesService ratesService;

    private final JabberwockyWordMapper jabberwockyMapper;

    @GetMapping("/random")
    public JabberwockyDto getRandom() {
        return jabberwockyMapper.apply(jabberwockyService.getRandom());
    }

    @GetMapping("/{id}")
    public JabberwockyDto get(@PathVariable final Long id) {
        return jabberwockyMapper.apply(jabberwockyService.get(id));
    }

    @PostMapping("/{id}/rate")
    @ResponseStatus(HttpStatus.OK)
    public JabberwockyDto rate(@NotNull
                               @JabberwockyWordExists
                               @PathVariable final Long id,
                               @NotBlank
                               @RequestParam final String userId,
                               @NotNull
                               @RequestParam final Boolean isPositive) {
        ratesService.rate(id, userId, isPositive);
        return jabberwockyMapper.apply(jabberwockyService.get(id));
    }

    @GetMapping
    public Page<JabberwockyDto> search(@NotBlank @RequestParam final String word,
                                       @Min(value = 1) @Max(value = 100)
                                       @RequestParam final Integer count,
                                       @Min(value = 0) final Integer page) {
        final Page<JabberwockyWord> pageResult = jabberwockyService.search(word, count, page);
        return new PageImpl<>(
                pageResult.getContent()
                        .stream()
                        .map(jabberwockyMapper::apply)
                        .collect(Collectors.toList()),
                pageResult.getPageable(),
                pageResult.getTotalElements()
        );
    }
}
