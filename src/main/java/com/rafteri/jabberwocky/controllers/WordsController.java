package com.rafteri.jabberwocky.controllers;

import com.rafteri.jabberwocky.services.WordsService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

/**
 * 03.02.2021
 *
 * @author Dinar Rafikov
 */
@RestController
@RequestMapping("/words")
@RequiredArgsConstructor
public class WordsController {

    private final WordsService wordsService;

    @GetMapping("/definition")
    public String getDefinition(@RequestParam(required = false) final String word,
                                @RequestParam(required = false) final Long id) {
        if (id != null) {
            return wordsService.getDefinition(id);
        } else if (StringUtils.isNotBlank(word)) {
            return wordsService.getDefinition(word);
        } else {
            throw new HttpClientErrorException(
                    HttpStatus.BAD_REQUEST,
                    "at least one param required"
            );
        }
    }
}
