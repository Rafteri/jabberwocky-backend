package com.rafteri.jabberwocky.utils;

import java.io.*;

/**
 * 30.01.2021
 *
 * @author Dinar Rafikov
 */
public class WordsToSqlInsert {

    public static void main(String[] args) throws IOException {
        definitionsToSql();
    }

    private static void wordsToSql() throws IOException {
        try (final BufferedReader br = new BufferedReader(new FileReader("words.txt"));
             final FileWriter fw = new FileWriter("words.sql")) {
            String line;
            fw.write("INSERT INTO words (id, word) VALUES ");
            boolean isFirst = true;
            while ((line = br.readLine()) != null) {
                if (isFirst) {
                    fw.write("\n");
                    isFirst = false;
                } else {
                    fw.write(",\n");
                }
                final String escapedLine = line.replace("'", "''");
                fw.write("(nextval('words_seq'), '" + escapedLine + "')");
            }
            fw.write(";");
        }
    }

    private static void wordsWithDefinitionsToSql() throws IOException {
        try (final BufferedReader br = new BufferedReader(new FileReader("words_with_definition.txt"));
             final FileWriter fw = new FileWriter("words_with_definition.sql")) {
            String line;
            fw.write("INSERT INTO words (id, word, definition) VALUES ");
            boolean isFirst = true;
            while ((line = br.readLine()) != null) {
                if (isFirst) {
                    fw.write("\n");
                    isFirst = false;
                } else {
                    fw.write(",\n");
                }
                final String escapedLine = line.replace("'", "''");
                final String[] parts = escapedLine.split(":");
                fw.write("(nextval('words_seq'), '" + parts[0] + "', '" + parts[1] + "')");
            }
            fw.write(";");
        }
    }

    private static void definitionsToSql() throws IOException {
        try (final BufferedReader br = new BufferedReader(new FileReader("words_with_definition.txt"));
             final FileWriter fw = new FileWriter("definitions.sql")) {
            String line;
            int id = 1;
            while ((line = br.readLine()) != null) {
                final String escapedLine = line.replace("'", "''");
                final String[] parts = escapedLine.split(":");
                fw.write("UPDATE words SET definition = '" + parts[1] + "' WHERE id = " + id + ";\n");
                id++;
            }
        }
    }
}
