package com.rafteri.jabberwocky.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * 03.02.2021
 *
 * @author Dinar Rafikov
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BadRequestException extends RuntimeException {

    public BadRequestException(final String msg) {
        super(msg);
    }
}
