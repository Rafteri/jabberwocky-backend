package com.rafteri.jabberwocky.repositories;

import com.rafteri.jabberwocky.models.JabberwockyRate;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 18.02.2021
 *
 * @author Dinar Rafikov
 */
public interface JabberwockyRatesRepository extends JpaRepository<JabberwockyRate, JabberwockyRate.Ids> {
}
