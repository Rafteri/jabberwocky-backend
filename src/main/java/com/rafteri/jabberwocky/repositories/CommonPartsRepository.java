package com.rafteri.jabberwocky.repositories;

import com.rafteri.jabberwocky.models.CommonPart;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 01.02.2021
 *
 * @author Dinar Rafikov
 */
public interface CommonPartsRepository extends JpaRepository<CommonPart, Long> {
}
