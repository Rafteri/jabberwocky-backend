package com.rafteri.jabberwocky.repositories;

import com.rafteri.jabberwocky.models.Word;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

/**
 * 01.02.2021
 *
 * @author Dinar Rafikov
 */
public interface WordsRepository extends JpaRepository<Word, Long> {

    @Query("select w.definition from Word w where w.word = :word")
    Optional<String> findDefinition(String word);

    @Query("select w.definition from Word w where w.id = :id")
    Optional<String> findDefinition(Long id);
}
