package com.rafteri.jabberwocky.repositories;

import com.rafteri.jabberwocky.models.JabberwockyWord;
import com.rafteri.jabberwocky.models.Word;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

/**
 * 01.02.2021
 *
 * @author Dinar Rafikov
 */
public interface JabberwockyWordsRepository extends JpaRepository<JabberwockyWord, Long> {

    Page<JabberwockyWord> findByComboLike(String query, Pageable pageable);

    @Query("select min(jw.id) from JabberwockyWord jw")
    Long lowestId();

    @Query("select max(jw.id) from JabberwockyWord jw")
    Long greatestId();

    @Modifying
    @Query("update JabberwockyWord set positiveRates = positiveRates + :posInterest, " +
            " negativeRates = negativeRates + :negInterest " +
            " where id = :id")
    void addRates(Long id, long posInterest, long negInterest);
}
