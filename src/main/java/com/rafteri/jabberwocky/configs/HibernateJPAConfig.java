package com.rafteri.jabberwocky.configs;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.domain.EntityScanPackages;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.orm.jpa.hibernate.SpringImplicitNamingStrategy;
import org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy;
import org.springframework.boot.task.TaskSchedulerBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.orm.jpa.persistenceunit.DefaultPersistenceUnitManager;
import org.springframework.orm.jpa.persistenceunit.PersistenceUnitManager;

import javax.persistence.spi.PersistenceUnitInfo;

import java.util.List;
import java.util.Map;

import static org.hibernate.cfg.AvailableSettings.*;

/**
 * @author Dmitry Sadchikov
 */
@Configuration
@EntityScan("com.rafteri.jabberwocky.models")
@AutoConfigureAfter({HibernateJpaAutoConfiguration.class})
public class HibernateJPAConfig {

    @Bean
    public EntityScanPackages entityScanPackages(final BeanFactory beanFactory) {
        return EntityScanPackages.get(beanFactory);
    }

    @Bean
    public Metadata getMetadata(final StandardServiceRegistry standardServiceRegistry,
                                final PersistenceUnitInfo persistenceUnitInfo) {
        final MetadataSources metadataSources = new MetadataSources(standardServiceRegistry);
        final List<String> managedClassNames = persistenceUnitInfo.getManagedClassNames();
        for (String managedClassName : managedClassNames) {
            metadataSources.addAnnotatedClassName(managedClassName);
        }
        return metadataSources.buildMetadata();
    }

    @Bean
    public StandardServiceRegistry getStandardServiceRegistry(final JpaProperties jpaProperties,
                                                              final DataSourceProperties dataSourceProperties) {
        final Map<String, String> properties = jpaProperties.getProperties();
        return new StandardServiceRegistryBuilder()
                .applySettings(properties)
                .applySetting(USER, dataSourceProperties.getUsername())
                .applySetting(PASS, dataSourceProperties.getPassword())
                .applySetting(URL, dataSourceProperties.getUrl())
                .applySetting(DRIVER, dataSourceProperties.getDriverClassName())
                .applySetting(IMPLICIT_NAMING_STRATEGY, SpringImplicitNamingStrategy.class.getCanonicalName())
                .applySetting(PHYSICAL_NAMING_STRATEGY, SpringPhysicalNamingStrategy.class.getCanonicalName())
                .build();
    }

    @Bean
    public PersistenceUnitInfo getPersistenceUnitInfo(final EntityScanPackages entityScanPackages) {
        final List<String> packagesToScan = entityScanPackages.getPackageNames();
        final DefaultPersistenceUnitManager persistenceUnitManager = new DefaultPersistenceUnitManager();
        String[] packagesToScanArr = packagesToScan.toArray(new String[0]);
        persistenceUnitManager.setPackagesToScan(packagesToScanArr);
        persistenceUnitManager.afterPropertiesSet();
        return persistenceUnitManager.obtainDefaultPersistenceUnitInfo();
    }

    @Bean
    @Primary
    public AsyncTaskExecutor asyncTaskExecutor() {
        return new TaskSchedulerBuilder()
                .poolSize(2)
                .build();
    }
}
