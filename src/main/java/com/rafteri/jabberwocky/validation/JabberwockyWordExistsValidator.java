package com.rafteri.jabberwocky.validation;

import com.rafteri.jabberwocky.repositories.JabberwockyWordsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * 18.02.2021
 *
 * @author Dinar Rafikov
 */
@Component
@RequiredArgsConstructor
public class JabberwockyWordExistsValidator implements ConstraintValidator<JabberwockyWordExists, Long> {

    private final JabberwockyWordsRepository wordsRepository;

    @Override
    public boolean isValid(final Long id, final ConstraintValidatorContext constraintValidatorContext) {
        return id == null || wordsRepository.existsById(id);
    }
}
