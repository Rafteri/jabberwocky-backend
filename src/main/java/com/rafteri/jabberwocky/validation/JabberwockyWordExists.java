package com.rafteri.jabberwocky.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * 18.02.2021
 *
 * @author Dinar Rafikov
 */
@Documented
@Constraint(validatedBy = JabberwockyWordExistsValidator.class)
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface JabberwockyWordExists {
    String message() default "not-found";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
